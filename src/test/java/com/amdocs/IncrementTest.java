package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testGetCounter() throws Exception {
        Increment increment = new Increment();
        assertEquals("Counter", 1, increment.getCounter());
        assertEquals("Counter", 2, increment.getCounter());
    }

    @Test
    public void testDecreaseCounterWhenInputIsZero() throws Exception {
        Increment increment = new Increment();
        assertEquals("Decrease Counter when input is 0", 1, increment.decreasecounter(0));
        assertEquals("Decrease Counter when input is 0 again", 0, increment.decreasecounter(0));
    }

    @Test
    public void testDecreaseCounterWhenInputIsOne() throws Exception {
        Increment increment = new Increment();
        assertEquals("Decrease Counter when input is 1", 1, increment.decreasecounter(1));
    }

    @Test
    public void testDecreaseCounterWhenInputIsTwo() throws Exception {
        Increment increment = new Increment();
        assertEquals("Decrease Counter when input is 2", 1, increment.decreasecounter(2));
        assertEquals("Decrease Counter when input is 2 again", 2, increment.decreasecounter(2));
    }

    @Test
    public void testDecreaseCounterWhenInputIsNegative() throws Exception {
        Increment increment = new Increment();
        assertEquals("Decrease Counter when input is negative", 1, increment.decreasecounter(-1));
        assertEquals("Decrease Counter when input is negative again", 2, increment.decreasecounter(-1));
    }
}

